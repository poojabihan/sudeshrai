//
//  SeggestionAndComplainParser.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit
import SwiftyJSON
class SeggestionAndComplainParser: NSObject {

    class func parseSuggestionAndGallery(response : JSON, completionHandler: @escaping ([GetSuggestionAndComplainModel]) -> Void) {
        
        var arr = [GetSuggestionAndComplainModel]()
        
        for json in response["data"].arrayValue {
            
            let model = GetSuggestionAndComplainModel()
            
            model.strFirstName = json["firstName"].stringValue
            model.strLastName = json["lastName"].stringValue
            model.strContact = json["contact"].stringValue
            model.strDescription = json["description"].stringValue
            model.strAddress = json["address"].stringValue
            model.strCreated_At = json["created_at"].stringValue
            model.strImage_Url = json["image_url"].stringValue
            model.strSubject = json["subject"].stringValue
            model.strType = json["type"].stringValue
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
}
