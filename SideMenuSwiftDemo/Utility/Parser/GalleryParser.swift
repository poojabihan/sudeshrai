//
//  GalleryParser.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit
import SwiftyJSON


class GalleryParser: NSObject {

    class func parseGalleryImages(response : JSON, completionHandler: @escaping ([GalleryModel]) -> Void) {
        
        var arr = [GalleryModel]()
        
        for json in response["data"].arrayValue {
            
            let model = GalleryModel()
            
            model.strImageUrl = json["ImageUrl"].stringValue
            arr.append(model)
        }
        
        completionHandler(arr)
    }
}
