//
//  NetworkConstant.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

let kBaseURL = "https://sudeshrai.com/sudesh-rai-api/?api_access="

class NetworkConstant: NSObject {
    
    struct Auth {
        static let login = "login"
    }
    
    struct Gallery{
        static let galleryImages = "galleryImages"

    }
    
    struct AddComplaintsOrSuggestions{
        
        static let addComplainSuggestions = "addComplainSuggestions"
        static let getComplainSuggestions = "getComplainSuggestions"
    }
}

    



