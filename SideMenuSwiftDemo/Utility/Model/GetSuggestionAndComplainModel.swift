//
//  GetSuggestionAndComplainModel.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit

class GetSuggestionAndComplainModel: NSObject {

    var strAddress = ""
    var strContact = ""
    var strCreated_At = ""
    var strDescription = ""
    var strFirstName = ""
    var strLastName = ""
    var strImage_Url = ""
    var strSubject = ""
    var strType = ""
    
}
