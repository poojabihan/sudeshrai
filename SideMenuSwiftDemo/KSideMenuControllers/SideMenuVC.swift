//
//  SideMenuVC.swift
//  SideMenuSwiftDemo
//
//  Created by Kiran Patel on 1/2/16.
//  Copyright © 2016  SOTSYS175. All rights reserved.
//

import Foundation
import UIKit
import JKNotificationPanel
class SideMenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet var tableView: UITableView!
    
    let aData : NSMutableArray = ["होम", "गैलरी", "संपर्क", "सोशल एकाउंट्स", "शिकायत/ सुझाव","एडमिन लॉग इन"]
    let aImageData : NSMutableArray = ["home", "gallery","IMP","social","Complaints","login"]
    
    let aDataT : NSMutableArray = ["होम", "गैलरी", "संपर्क", "सोशल एकाउंट्स", "शिकायत/ सुझाव","लॉग आउट"]
    let aImageDataT : NSMutableArray = ["home", "gallery","IMP","social","Complaints","logout"]
    
    var logout = Bool()
    var panel = JKNotificationPanel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView()

        
         NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.reloadTableView(_:)), name: NSNotification.Name(rawValue: "RefreshSideBar"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.reloadTableViewWhenLogout(_:)), name: NSNotification.Name(rawValue: "RefreshSideBarWhenLogout"), object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }

    @objc func reloadTableView(_ notification:Notification) {
        
        UserDefaults.standard.set(true, forKey: "logout")
    }
    
    @objc func reloadTableViewWhenLogout(_ notification:Notification) {
        
        UserDefaults.standard.set(false, forKey: "logout")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  UserDefaults.standard.bool(forKey: "logout") == true{
            
            return aDataT.count

        }
        else{
            
        return aData.count
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  UserDefaults.standard.bool(forKey: "logout") == true{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "KCell") as! KCell
            
            cell.imgTitle.image = UIImage(named: aImageDataT[indexPath.row] as! String)
            cell.lblTitle.text = aDataT[indexPath.row] as? String
            cell.lblDidSelectGreen.tag = indexPath.row
            
            return cell
        }
        else{
            
           let cell = tableView.dequeueReusableCell(withIdentifier: "KCell") as! KCell
        
            cell.imgTitle.image = UIImage(named: aImageData[indexPath.row] as! String)
            cell.lblTitle.text = aData[indexPath.row] as? String
            cell.lblDidSelectGreen.tag = indexPath.row
        
        return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "KCell") as! KCell
        
        cell.lblDidSelectGreen.addTarget(self, action: #selector(self.pressButton(_:)), for: .touchUpInside)
        
       
        
        if indexPath.row == 0 {
            kConstantObj.SetIntialMainViewController("HomeView")
            
        }
        else if indexPath.row == 1 {
            
            kConstantObj.SetIntialMainViewController("GalleryView")
            
            
        }
        else if indexPath.row == 2 {
            
            kConstantObj.SetIntialMainViewController("ImportantContactsView")
            
        }
        else if indexPath.row == 3 {
            
            kConstantObj.SetIntialMainViewController("SocialAccountView")
            
        }
        else if indexPath.row == 4 {
            
            kConstantObj.SetIntialMainViewController("ComplaintView")
            
        }
        else if indexPath.row == 5 {
            
            if  UserDefaults.standard.bool(forKey: "logout") == true {
                
                let alertController = UIAlertController(title: "लॉग आउट", message: "क्या आप निश्चित रूप से लॉग आउट करना चाहते हैं!", preferredStyle: UIAlertControllerStyle.alert)
                
                alertController.addAction(UIAlertAction(title: "हाँ", style: .default, handler: { action in
                    //run your function here
                    self.goToLoginScreen()
                }))
                
                let cancelAction = UIAlertAction(title: "नहीं", style: .default, handler: nil)
                
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                
                kConstantObj.SetIntialMainViewController("LoginView")
            }
        }
        
        
    }

    func goToLoginScreen(){
        
        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)

        self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "आप सफलतापूर्वक लोग आउट कर चुके है")
        

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshSideBarWhenLogout"), object: nil)
        
        kConstantObj.SetIntialMainViewController("HomeView")

    }
    
    //The target function
    @objc func pressButton(_ sender: UIButton){ //<- needs `@objc`
        print("\(sender)")
        let index = IndexPath(row: sender.tag, section: 0)
        let cell: KCell = self.tableView.cellForRow(at: index) as! KCell
        
     cell.isHidden = false
        
    }
    
}
