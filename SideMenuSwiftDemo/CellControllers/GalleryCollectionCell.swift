//
//  GalleryCollectionCell.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit

class GalleryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgForGallery: UIImageView!
}
