//
//  SuggestionViewCell.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 31/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit

class SuggestionViewCell: UITableViewCell {

    @IBOutlet weak var lblSuggestionNumber: UILabel!
     @IBOutlet weak var lblSuggestionDate: UILabel!
     @IBOutlet weak var lblSuggestionDescription: UILabel!
     @IBOutlet weak var lblPersonName: UILabel!
     @IBOutlet weak var lblPersonNumber: UILabel!
     @IBOutlet weak var lblPersonAddress: UILabel!
    @IBOutlet weak var heightForSuggestionLbl: NSLayoutConstraint!
    @IBOutlet weak var img2: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    
}
