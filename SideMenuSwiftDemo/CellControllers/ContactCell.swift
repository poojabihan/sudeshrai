//
//  ContactCell.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {

    @IBOutlet weak var imgTitle: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
