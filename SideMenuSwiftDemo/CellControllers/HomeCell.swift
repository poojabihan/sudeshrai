//
//  HomeCell.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var heightConstraintForLbl: NSLayoutConstraint!
    
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
