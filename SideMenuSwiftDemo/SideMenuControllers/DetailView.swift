//
//  DetailView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit

class DetailView: UIViewController {
    
    @IBOutlet weak var btnZoom: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblPersonName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblSuggestionSubject: UILabel!
    @IBOutlet weak var heightOflblDescription: NSLayoutConstraint!
    @IBOutlet weak var heightOflblAddress: NSLayoutConstraint!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    var dictOfselectedIndex = GetSuggestionAndComplainModel()
    var floatheightlblDescription = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblSuggestionSubject.text = dictOfselectedIndex.strSubject
        lblDate.text = dictOfselectedIndex.strCreated_At
        
        lblDescription.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblDescription.numberOfLines = 0
        
        lblAddress.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblAddress.numberOfLines = 2
        
        heightOflblAddress.constant = (lblAddress.text?.height(withConstrainedWidth: lblAddress.frame.size.width, font: UIFont.systemFont(ofSize: 14)))!
        
        floatheightlblDescription = (lblDescription.text?.height(withConstrainedWidth: lblDescription.frame.size.width, font: UIFont.systemFont(ofSize: 18)))!
        
        heightOflblDescription.constant = floatheightlblDescription
        lblDescription.text = dictOfselectedIndex.strDescription
        lblPersonName.text = dictOfselectedIndex.strFirstName + " " + dictOfselectedIndex.strLastName
        lblContact.text = dictOfselectedIndex.strContact
        lblAddress.text = dictOfselectedIndex.strAddress
        
        let url = URL(string: dictOfselectedIndex.strImage_Url)
        if dictOfselectedIndex.strImage_Url == ""{
            
            imgView.isHidden = true
            btnZoom.isHidden = true
            btnZoom.isUserInteractionEnabled = false
        }
        else{
            imgView.layer.cornerRadius = 7
            imgView.borderColor = UIColor(hexString: "535353")
            btnZoom.isHidden = false
            imgView.isHidden = false
            btnZoom.isUserInteractionEnabled = true
            imgView.borderWidth = 0.5
            imgView.clipsToBounds = true
            imgView.kf.setImage(with: url, placeholder: UIImage(named :"no_image"), progressBlock:nil, completionHandler: nil)
        }
        
        if dictOfselectedIndex.strType == "complain"{
            
            lblTitle.text = "शिकायत"
        }
        else{
            lblTitle.text = "सुझाव"

            
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnZoomAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomView") as! ImageZoomView
        
        objVC.strImage = dictOfselectedIndex.strImage_Url
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
