//
//  GalleryView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

class GalleryView: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource{
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var imgForDidSelect: UIImageView!
    
    var arrForGalleryImages = [GalleryModel]()
 
    let panel = JKNotificationPanel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgForDidSelect.layer.cornerRadius = 5
        imgForDidSelect.clipsToBounds = true
        
        callWebServiceToGetGalleryImages()

        //To manage the cell constaint of Collection_Values
        let cellSize = CGSize(width: 110 , height:110)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        layout.minimumLineSpacing = 2.0
        layout.minimumInteritemSpacing = 0
        galleryCollectionView.setCollectionViewLayout(layout, animated: true)
        galleryCollectionView.reloadData()
        
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    //MARK: - UIcollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrForGalleryImages.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionCell", for: indexPath as IndexPath) as! GalleryCollectionCell
        
        let model = arrForGalleryImages[indexPath.row]
        let url = URL(string: model.strImageUrl)
        cell.imgForGallery.kf.setImage(with: url, placeholder: UIImage(named :"no_image"), progressBlock:nil, completionHandler: nil)
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let model = arrForGalleryImages[indexPath.row]
        let url = URL(string: model.strImageUrl)
        self.imgForDidSelect.kf.setImage(with: url, placeholder: UIImage(named: "no_image"), progressBlock:nil, completionHandler: nil)
    }
    
    func callWebServiceToGetGalleryImages() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Gallery.galleryImages, param: nil, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    GalleryParser.parseGalleryImages(response: response!, completionHandler: { (arrOfGallerymages) in
                        
                        self.arrForGalleryImages = arrOfGallerymages
                    })
                    
                    self.galleryCollectionView.reloadData()
                    let model = self.arrForGalleryImages[0]
                    let url = URL(string: model.strImageUrl)
                    self.imgForDidSelect.kf.setImage(with: url, placeholder: UIImage(named: "no_image"), progressBlock:nil, completionHandler: nil)
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
}
