//
//  ImageZoomView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit
import Kingfisher
class ImageZoomView : UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    var strImage = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
         let url = URL(string: strImage)
        imgView.kf.setImage(with: url, placeholder: UIImage(named :"no_image"), progressBlock:nil, completionHandler: nil)
        
    }
    @IBAction func btnCrossAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
}
