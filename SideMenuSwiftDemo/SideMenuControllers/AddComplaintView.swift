//
//  AddComplaintView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 31/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit
import Photos
import NVActivityIndicatorView
import JKNotificationPanel


class AddComplaintView: UIViewController , UITextFieldDelegate, UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtSurname: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtSubjectOfSuggestion: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var textField: UITextField?
    var imagePicker = UIImagePickerController()
    var strsegmentTitle = ""
    let panel = JKNotificationPanel()
    var doNotSendImage = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if strsegmentTitle == "complain" {
            
            lblTitle.text = "शिकायत दर्ज करें"
        }
        else{
            lblTitle.text = "सुझाव दर्ज करें"
        }
        
        imagePicker.delegate = self
        doNotSendImage = true

        
        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(AddComplaintView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddComplaintView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        txtDescription.text = "विस्तार में बताएं..."
        txtDescription.layer.borderColor = UIColor(hexString: "535353").cgColor
        txtDescription.font = UIFont.systemFont(ofSize: 20)
        txtDescription.textColor = UIColor.lightGray
        txtDescription.layer.borderWidth = 1
        txtDescription.returnKeyType = .done
        txtDescription.delegate = self
    
        tblView.setNeedsDisplay()
    }
    
  
    
    
    /*func textFiledPlaceHolderFontStyle(strPlaceHolderText : String) -> NSMutableAttributedString {
        
        var myMutableStringTitle = NSMutableAttributedString()
        
        myMutableStringTitle = NSMutableAttributedString(string:strPlaceHolderText, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 20)])
        
        myMutableStringTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range:NSRange(location:0, length: strPlaceHolderText.count)) // Color
        
        return myMutableStringTitle
    }*/
    
    
    
    //MARK: - IBActions
    @IBAction func btnSaveAction(_ sender: Any) {
        
            if (txtMobileNumber.text?.isEmpty)! {
            let alert  = UIAlertController(title: "", message: "कृपया फ़ोन नंबर डाले", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ओके", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if (txtMobileNumber.text?.count != 10) {
                let alert  = UIAlertController(title: "", message: "कृपया दस अंको का फ़ोन नंबर डाले", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ओके", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
                
            else if (txtName.text?.isEmpty)! {
                
                let alert  = UIAlertController(title: "", message: "कृपया नाम डाले", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ओके", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            else if (txtSurname.text?.isEmpty)! {
                let alert  = UIAlertController(title: "", message: "कृपया सरनाम डाले", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ओके", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
        
            else if (txtAddress.text?.isEmpty)! {
                let alert  = UIAlertController(title: "", message: "कृपया पता डाले", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ओके", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
            else if (txtSubjectOfSuggestion.text?.isEmpty)! {
                let alert  = UIAlertController(title: "", message: "कृपया शीर्षक डाले", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ओके", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
            else if (txtDescription.text?.isEmpty)! {
                let alert  = UIAlertController(title: "", message: "कृपया विस्तार से बतायें ", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ओके", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
            else{
                
                if strsegmentTitle == "complain"{
                    
                    callWebServiceForAddComplaintOrSuggestion()
                   
             }
                else{
                    callWebServiceForAddComplaintOrSuggestion()
                    
                }
        }
            
    }
    
    func ReloadView() {
        
        txtMobileNumber.text = ""
        txtName.text = ""
        txtSurname.text = ""
        txtAddress.text = ""
        txtDescription.text = ""
        txtSubjectOfSuggestion.text = ""
    }
    
    @IBAction func btnAddPictureAction(_ sender: Any) {
            
            let alert = UIAlertController(title: "इनमें से किसी भी विकल्प से अपनी फ़ाइल चुनें", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "कैमरा", style: .default, handler: { _ in
                self.openCamera()
                self.doNotSendImage = false

            }))
            
            alert.addAction(UIAlertAction(title: "गैलरी", style: .default, handler: { _ in
                self.openGallary()
                self.doNotSendImage = false

            }))
            
            alert.addAction(UIAlertAction.init(title: "रद्द करें", style: .cancel, handler: nil))
            
            /*If you want work actionsheet on ipad
             then you have to use popoverPresentationController to present the actionsheet,
             otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as? UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }
            
            self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Custom Functions
    
    /*Action Sheet Options Function for Uploading File*/
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "वार्निंग", message: "आपके पास कैमरा नहीं है", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
            
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                imageView.image = image
                self.dismiss(animated: false, completion: nil)
            }
                
            else{
                
                let alert  = UIAlertController(title: "वार्निंग", message: "यह छवि का समर्थन नहीं करेगा।", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            
                picker.popViewController(animated: true)
                print("Something went wrong in  image")
            }
            
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    
    //MARK: - UITextFiled keyboard hide unhide methods
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- UITextViewDelegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "विस्तार में बताएं..." {
            textView.text = ""
            txtDescription.font = UIFont.systemFont(ofSize: 20)
            txtDescription.textColor = UIColor.lightGray
        }
       
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
            txtDescription.font = UIFont.systemFont(ofSize: 20)
            textView.textColor = UIColor(hexString: "535353")
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            textView.text = "विस्तार में बताएं..."
            txtDescription.font = UIFont.systemFont(ofSize: 20)
            txtDescription.textColor = UIColor.lightGray

        }
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = UIImagePNGRepresentation(image)!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    func callWebServiceForAddComplaintOrSuggestion(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        var param = [String : Any]()
        if doNotSendImage == true{
            param =
            ["device_id": UIDevice.current.identifierForVendor?.uuidString ?? "",
            "type": strsegmentTitle,
            "firstName": txtName.text!,
            "lastName": txtSurname.text!,
            "address": txtAddress.text!,
            "contact": txtMobileNumber.text!,
            "subject": txtSubjectOfSuggestion.text!,
            "description" : txtDescription.text! ,
            "created_at": "",
            "image_url": ""
            ]
            
        }
        else{
            if imageView.image ==  #imageLiteral(resourceName: "no_image") {
                param =
                    ["device_id": UIDevice.current.identifierForVendor?.uuidString ?? "",
                     "type": strsegmentTitle,
                     "firstName": txtName.text!,
                     "lastName": txtSurname.text!,
                     "address": txtAddress.text!,
                     "contact": txtMobileNumber.text!,
                     "subject": txtSubjectOfSuggestion.text!,
                     "description" : txtDescription.text! ,
                     "created_at": "",
                     "image_url": ""
                ]
            }
            else{
                /*C2DAFBE7-BE85-4946-99BF-686B6DB92B98*/
            param =
            ["device_id": UIDevice.current.identifierForVendor?.uuidString ?? "",
            "type": strsegmentTitle,
            "firstName": txtName.text!,
            "lastName": txtSurname.text!,
            "address": txtAddress.text!,
            "contact": txtMobileNumber.text!,
            "subject": txtSubjectOfSuggestion.text!,
            "description" : txtDescription.text! ,
            "created_at": "",
            "image_url": convertImageToBase64(image: imageView.image!)]
            }
        }
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AddComplaintsOrSuggestions.addComplainSuggestions, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                
                    self.ReloadView()
                    self.navigationController?.popViewController(animated: true)
                    if self.strsegmentTitle == "complain" {
                        
                       self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "आपकी शिकायत दर्ज की जा चुकी है")
                    }
                    else{
                        self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "आपका सुझाव दर्ज किया जा चूका है")

                    }
                    
                }
                else {
                    self.ReloadView()
                    print(errorMsg ?? "")
                     if self.strsegmentTitle == "complain" {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "शिकायत जमा नहीं हो पाया")
                    }
                     else{
                        
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "सुझाव जमा नहीं हो पाया")
                    }
                }
            }
        }
        
        
        
        
    }
}
