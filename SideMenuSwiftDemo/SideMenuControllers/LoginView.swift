//
//  LoginView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
class LoginView: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    let panel = JKNotificationPanel()
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        view1.layer.cornerRadius = 5
        view1.clipsToBounds = true
        
        view2.layer.cornerRadius = 5
        view2.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }

    @IBAction func btnLoginAction(_ sender: Any) {
        
        if (txtEmail.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "कृपया उपयोगकर्ता नाम दर्ज करें।")
        }
            
        else if (txtEmail.text?.isValidEmail())! {
            if (txtPassword.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "कृप्या पास्वर्ड भरो।")
            }
            else if(checkForSpace(strCheckString: txtPassword.text!) ==  true){
                
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "पासवर्ड फ़ील्ड में व्हाइट स्पेस की अनुमति नहीं है।")
            }
            else {
                callWebServiceForLogin()
            }
        }
        else {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "कृपया मान्य उपयोगकर्ता नाम दर्ज करें।")
        }
    }
    
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    func callWebServiceForLogin() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        
        let param = ["username": txtEmail.text ?? "" ,
                     "password":txtPassword.text ?? ""
        ] as [String:Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.login, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                   
                    kConstantObj.SetIntialMainViewController("HomeView")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshSideBar"), object: nil)
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "आप सफलतापूर्वक लोगिन कर चुके है")
                   
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "आईडी पासवर्ड की दोबारा जाँच करे")
                    self.txtPassword.text = ""
                }
            }
        }
    }
    //MARK: - UITextfiled Delegete
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}

