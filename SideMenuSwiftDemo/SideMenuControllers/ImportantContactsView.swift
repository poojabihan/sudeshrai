//
//  ImportantContactsView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit
import MessageUI

class ImportantContactsView: UIViewController, UITableViewDataSource,UITableViewDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrOfImpContacts = ["+9198260-52417","+9198260-52417", "sudesh_rai1969", "+9198260-52417", "sudeshraisehore@gmail.com", "राय विला, पुरानी होटल क्रिसेंट के पीछे, इंदौर भोपाल रोड, सीहोर, एम.पी. 466001", "www.sudeshrai.com"]
    
   var arrOfImages = ["telephone", "whatsapp", "instagram", "SMS", "mail", "pin", "web" ]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()

        tableView.layer.cornerRadius = 5
        tableView.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfImpContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        
        cell.imgTitle.image = UIImage(named: arrOfImages[indexPath.row])
        
        cell.lblTitle.text = arrOfImpContacts[indexPath.row]
        
        if indexPath.row == 5{
            
            cell.lblTitle.numberOfLines = 2
            cell.lblTitle.lineBreakMode = .byWordWrapping
            cell.lblTitle.font = UIFont.systemFont(ofSize: 15)
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            if let url = NSURL(string: "tel://\(9826052417)"), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }
        
        else if indexPath.row == 1{
            let urlWhats = "whatsapp://send?phone=+919826052417&abid=12354&text="
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = URL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL) {
                        UIApplication.shared.openURL(whatsappURL)
                    } else {
                        print("Install Whatsapp")
                    }
                }
            }
        }
        else if indexPath.row == 2 {
            var instagramHooks = "https://www.instagram.com/sudeshraiofficial/"
            var instagramUrl = NSURL(string: instagramHooks)
            if UIApplication.shared.canOpenURL(instagramUrl! as URL) {
                UIApplication.shared.openURL(instagramUrl! as URL)
            } else {
                //redirect to safari because the user doesn't have Instagram
                UIApplication.shared.openURL(NSURL(string: "http://instagram.com/")! as URL)
            }
        }
        else if indexPath.row == 3 {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = ["+919826052417"]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
        else if indexPath.row == 4 {
            let email = "sudeshraisehore@gmail.com"
            if let url = URL(string: "mailto:\(email)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        else if indexPath.row == 6 {
            guard let url = URL(string: "https://www.sudeshrai.com") else { return }
            UIApplication.shared.open(url)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    func call(phoneNumber: String) {
        if let url = URL(string: phoneNumber) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(phoneNumber): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(phoneNumber): \(success)")
            }
        }
    }
    

}
