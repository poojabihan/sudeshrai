//
//  HomeView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit

class HomeView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var floatheightTxtDescroption = CGFloat()
    let arrForTitle : NSArray = ["शिक्षा एवं संस्कार","राजनीति" ]
    
    let arrForDescription = [ "इंदौर के डेली कॉलेज और पुणे के डीवाय पाटिल कॉलेज जैसे उच्च प्रतिष्ठित संस्थानों से स्नातकोत्तर तथा एमबीए की शिक्षा प्राप्त सुदेश जी बचपन से ही अपने बाबूजी की दो एकदम विपरीत विरासतों के साथ बड़े हुए – एक धनाढ्य परिवार की संपन्न व्यवसायिक विरासत तथा दूसरी अपने द्वार पर मदद मांगने आये किसी भी व्यक्ति को खाली हाथ न जाने देने की उदारता से भरी विरासतl सुदेश जी ने पारिवारिक व्यवसाय की जमी जमाई विरासत व सुख सुविधा को छोड़कर अपने अंतर्मन की आवाज सुनते हुए बाबूजी की दूसरी विरासत को आगे बढ़ाने का निश्चय किया तथा जनसेवा की कठिन राह चुनीl",
                              
        "व्यापक रूप से जनसेवा करने हेतु सुदेश राय जी ने अंततः राजनीति में कदम रखा और सन 2008 में अपना पहला चुनाव लड़ाl हाँलाकि यह चुनाव तो वह हार गए लेकिन अपनी जनसेवा के चलते लोगों का दिल जीतने में जरूर कामयाब रहेl और इसी की बदौलत 2013 में निर्दलीय के रूप में चुनाव जीतकर विधायक बनेंl विधायक बनने के पश्चात जनसेवा हेतु प्रदेश के मुख्यमंत्री शिवराज सिंह चौहान का आशीर्वाद लगातार मिलने से सुदेश राय जी ने भाजपा को समर्थन दिया तथा आज भाजपा के साथ एक समर्पित सिपाही के तौर पर अपने विधानसभा क्षेत्र में जनसेवा करते हुए विकास की एक नई इबारत लिख रहे हैl" ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.layer.cornerRadius = 5
        mainView.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell
        
        
        cell.lblTitle.text = arrForTitle[indexPath.row] as? String
        cell.lblDescription.text = arrForDescription[indexPath.row]
        
        cell.lblDescription.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.lblDescription.numberOfLines = 0
        
        floatheightTxtDescroption = (cell.lblDescription.text?.height(withConstrainedWidth: cell.lblDescription.frame.size.width, font: UIFont.systemFont(ofSize: 18)))!
        
        cell.heightConstraintForLbl.constant = floatheightTxtDescroption
        
        if indexPath.row == 0 {
            //To remove seprator from particular cell
            tblView.separatorStyle = .none
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return floatheightTxtDescroption + 50
    }
    
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
