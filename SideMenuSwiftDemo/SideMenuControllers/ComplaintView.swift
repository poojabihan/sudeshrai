//
//  ComplaintView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 31/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
class ComplaintView: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var segControl: UISegmentedControl!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    /*
     complain
     suggetion
     */
    var strsegmentTitle = "complain"
    let panel = JKNotificationPanel()
    var arrOfList = [GetSuggestionAndComplainModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.tableFooterView = UIView()

        lblTitle.text = "शिकायत"
        tblView.layer.cornerRadius = 5
        tblView.clipsToBounds = true
        let font = UIFont.systemFont(ofSize: 15)
        segControl.setTitleTextAttributes([NSAttributedStringKey.font: font],
                                          for: .normal)
        if UserDefaults.standard.bool(forKey: "logout") == true {
            btnAdd.isHidden = true
        }
        else{
            
            btnAdd.isHidden = false

        }
        callWebServiceForgetComplaintOrSuggestion()
    }
    override func viewWillAppear(_ animated: Bool) {
        callWebServiceForgetComplaintOrSuggestion()

    }

    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }

    @IBAction func segControlAction(_ sender: Any) {
        
        switch segControl.selectedSegmentIndex {
        case 0:
            
            strsegmentTitle = "complain"
           
            lblTitle.text = "शिकायत"
        callWebServiceForgetComplaintOrSuggestion()
        case 1 :
            
            strsegmentTitle = "suggetion"
           
            lblTitle.text = "सुझाव"
         callWebServiceForgetComplaintOrSuggestion()
            
        default:
            strsegmentTitle = "complain"
            callWebServiceForgetComplaintOrSuggestion()
        }
    }
    
    @IBAction func btnAddComplaint(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddComplaintView") as! AddComplaintView
        objVC.strsegmentTitle = strsegmentTitle
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    //MARK: - TableView Datasource and delegate
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionViewCell") as! SuggestionViewCell

        let model = arrOfList[indexPath.row]

        cell.lblSuggestionDescription.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.lblSuggestionDescription.numberOfLines = 0
        
        cell.lblPersonAddress.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.lblPersonAddress.numberOfLines = 2
        
        cell.lblSuggestionNumber.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.lblSuggestionNumber.numberOfLines = 0
        
        cell.heightForSuggestionLbl.constant = (cell.lblSuggestionNumber.text?.height(withConstrainedWidth: cell.lblSuggestionNumber.frame.size.width, font: UIFont.systemFont(ofSize: 16)))!
            
        cell.lblSuggestionNumber.text = model.strSubject
        cell.lblSuggestionDate.text = model.strCreated_At
        cell.lblSuggestionDescription.text = model.strDescription
        cell.lblPersonName.text = model.strFirstName + " " + model.strLastName
        cell.lblPersonNumber.text = model.strContact
        cell.lblPersonAddress.text = model.strAddress
        
        
        let url = URL(string: model.strImage_Url)
        if model.strImage_Url == ""{
            
            cell.img2.isHidden = true
            
        }
        else{
        cell.img2.layer.cornerRadius = 7
        cell.img2.borderColor = UIColor(hexString: "535353")
        cell.img2.borderWidth = 0.5
        cell.img2.clipsToBounds = true
        cell.img2.kf.setImage(with: url, placeholder: UIImage(named :"no_image"), progressBlock:nil, completionHandler: nil)
        }
        return cell
    }
    
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Didselect")
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailView") as! DetailView
        
        objVC.dictOfselectedIndex = arrOfList[indexPath.row]
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    
    func callWebServiceForgetComplaintOrSuggestion(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        /*{
         "device_id":"",
         "type":"suggetion"
         }*/
        var param = [String : Any]()
        if UserDefaults.standard.bool(forKey: "logout") == true {
            
            param =  ["device_id": "",
                      "type": strsegmentTitle]
        }
        else{
            
           param =  ["device_id": UIDevice.current.identifierForVendor?.uuidString ?? "",
             "type": strsegmentTitle ]
        }
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.AddComplaintsOrSuggestions.getComplainSuggestions, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                  SeggestionAndComplainParser.parseSuggestionAndGallery(response: response!, completionHandler: { (arrOfList) in
                        
                        self.arrOfList  = arrOfList
                    })
                    self.tblView.reloadData()
                    
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
        
        
        
    }
}
