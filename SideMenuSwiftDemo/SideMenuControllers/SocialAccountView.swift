//
//  SocialAccountView.swift
//  SideMenuSwiftDemo
//
//  Created by Apple on 31/08/18.
//  Copyright © 2018  SOTSYS175. All rights reserved.
//

import UIKit

class SocialAccountView: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.webView.frame = self.view.bounds
        //self.webView.scalesPageToFit = true
        
        webView.layer.borderColor = UIColor(hexString: "535353").cgColor
        webView.layer.borderWidth = 1
        webView.layer.cornerRadius = 5
        webView.clipsToBounds = true

        
        webView.delegate = self
        let url = URL(string: "https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/SudeshRaiSehore/?ref=br_rs&tabs=timeline&width=320&height=650&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId")
        let request = URLRequest(url: url!)
        webView.loadRequest(request)

        // Do any additional setup after loading the view.
    }

    
    @IBAction func btnSocialAccountsAction(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            let url = URL(string: "https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/SudeshRaiSehore/?ref=br_rs&tabs=timeline&width=555&height=650&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId")
            let request = URLRequest(url: url!)
            webView.loadRequest(request)
            
            
        }
        
        if sender.tag == 2 {
            
            let url = URL(string: "https://twitter.com/Sudeshr52417")
            let request = URLRequest(url: url!)
            webView.loadRequest(request)
    }
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("web view start loading")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("web view load completely")
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("web view loading fail : ",error.localizedDescription)
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
}
